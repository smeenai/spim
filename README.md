This is a fork of the [SPIM](http://spimsimulator.sourceforge.net/) MIPS simulator.

Notable changes:

* The exception handler is embedded into the executable to prevent a file dependency
* The `-file` parameter can take multiple files
* An `-args` parameter was added to pass arguments to the files being run
* The simulation exits as soon as any error is encountered